# The FAQ house #

### What is this? ###
This is a little program for computercraft, mainly aimed at tekkit classic or other modpacks with cc and ic2.

### Why? ###
Why what?

### Why did you make this? ###
Why not?

### You expect me to type all of that out?!? ###
Nope. That's why it's on pastebin. If you/the server has the http api (and thus pastebin), you can simply do that. But be warned, it is probably slightly outdated, as I will only update it once in a while.

### Pastebin? ###
Click [here](pastebin.com/XSjFaaQK) to go to the pastebin page. Remember, it probably isn't up to date

# The EAQ house #

### Will you marry me? <3 ###
Who are you and what are you doing in my house?!?
